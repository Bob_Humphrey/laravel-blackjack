<x-layouts.base>
  <div class="font-nunito_regular">

    @include('layouts.flash')

    <main class="w-full">

      {{ $slot }}

    </main>

    @include('layouts.footer')
  </div>
</x-layouts.base>
