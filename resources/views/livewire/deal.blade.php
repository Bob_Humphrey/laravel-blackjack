<div class="{{ $gameOver ? 'hidden' : '' }}">
  <div
    class="flex flex-col lg:flex-row items-center justify-center w-full font-nunito_bold py-4 border-b border-gray-400">
    <button {{ $buttons['dealDisabled'] }} wire:click="deal" class=" btn">
      Deal
    </button>
  </div>
</div>
