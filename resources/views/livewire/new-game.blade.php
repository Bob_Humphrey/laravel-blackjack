<div class="{{ $gameOver ? '' : 'hidden' }}">
  <div class="font-gothic text-4xl text-amber-800 text-center tracking-wide uppercase pt-3">
    Game Over
  </div>
  <div
    class="flex flex-col lg:flex-row items-center justify-center w-full font-nunito_bold py-4 border-b border-gray-400">
    <button wire:click="newGame" class="btn">
      Start New Game
    </button>
  </div>
</div>
