<div class="">
  <div class="w-full">
    <x-header :state="$state" />
  </div>
  <div class="grid grid-cols-2 gap-16 w-3/4 font-nunito_regular text-gray-700 mx-auto">
    <div class="col-span-1">
      <x-dealer :state="$state" />
      <x-player1 :state="$state" />
      <x-player2 :state="$state" />
    </div>
    <div class="col-span-1">
      <x-score :state="$state" />
      <x-bet :state="$state" />
      <livewire:bet :state="$state" />
      <livewire:deal :state="$state" />
      <livewire:play-hand :state="$state" />
      <x-settlement :state="$state" />
      <livewire:new-game :state="$state" />
    </div>
  </div>
</div>
