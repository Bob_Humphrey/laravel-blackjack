<div class="{{ $gameOver ? 'hidden' : '' }}">
  <div
    class="flex flex-col lg:flex-row items-center justify-center w-full font-nunito_bold py-4 border-b border-gray-400">
    <button wire:key="stand" {{ $buttons['standDisabled'] }} wire:click="stand" class=" btn">
      Stand
    </button>
    <button wire:key="hit" {{ $buttons['hitDisabled'] }} wire:click="hit" class="btn">
      Hit
    </button>
    <button wire:key="split" {{ $buttons['splitDisabled'] }} wire:click="split" class="btn">
      Split
    </button>
    <button wire:key="double" {{ $buttons['doubleDisabled'] }} wire:click="double" class="btn">
      Double
    </button>
    <button wire:key="insure" {{ $buttons['insureDisabled'] }} wire:click="insure" class="btn">
      Insurance
    </button>
  </div>
</div>
