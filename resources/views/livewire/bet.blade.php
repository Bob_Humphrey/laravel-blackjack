<div class="{{ $gameOver ? 'hidden' : '' }}">
  <div
    class="flex flex-col lg:flex-row items-center justify-center w-full font-nunito_bold py-4 border-b border-gray-400">
    <button {{ $buttons['bet2Disabled'] }} wire:click="setBet(2)" class=" btn">
      Bet 2
    </button>
    <button {{ $buttons['bet10Disabled'] }} wire:click="setBet(10)" class="btn">
      Bet 10
    </button>
    <button {{ $buttons['bet20Disabled'] }} wire:click="setBet(20)" class="btn">
      Bet 20
    </button>
    <button {{ $buttons['bet50Disabled'] }} wire:click="setBet(50)" class="btn">
      Bet 50
    </button>
  </div>
</div>
