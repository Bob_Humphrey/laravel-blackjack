@php
use Carbon\Carbon;
$tomorrow = Carbon::tomorrow();
$score = 1000;
$bet = 2;
@endphp

<x-layouts.app>

  <div class="grid grid-cols-2 gap-8 w-3/4 font-nunito_regular text-gray-700 mx-auto">
    <div class="col-span-1">
      <x-dealer />
      <x-player1 />
      <x-player2 />
    </div>
    <div class="col-span-1">
      <x-score :amount="$score" />
      <x-bet :amount="$bet" />
    </div>
  </div>

  <div class="mt-64">
    <livewire:example>

      <div class="mt-10">
        EXAMPLE BLADE-UI-KIT COMPONENT
        COUNTDOWN UNTIL MIDNIGHT
        <x-countdown :expires="$tomorrow" />
      </div>
  </div>

</x-layouts.app>
