<div class="border-b border-gray-400">
  <div class="flex justify-between font-gothic text-4xl text-gray-700 tracking-wide uppercase">
    <div class="text-gray-700">
      Dealer
    </div>
    <div>
      {{ $hand['status'] }}
    </div>
  </div>
  @if ($hand['cards'])
    @if ($showHoleCard)
      <div class="flex pl-14">
        @foreach ($hand['cards'] as $card)
          <img src="{{ 'https://images.bob-humphrey.com/cards/' . $card['name'] . '.png' }}" alt=""
            class="w-24 -ml-14 pb-2">
        @endforeach
      </div>
    @else
      <div class="flex pl-14">
        <img src="{{ 'https://images.bob-humphrey.com/cards/' . $hand['cards'][0]['name'] . '.png' }}" alt=""
          class="w-24 -ml-14 pb-2">
        <img src="{{ 'https://images.bob-humphrey.com/cards/HIDDEN-LIGHT-GRAY.png' }}" alt="" class="w-24 -ml-14 pb-2">
      </div>
    @endif
  @else
    <img src="{{ 'https://images.bob-humphrey.com/cards/BLANK.png' }}" alt="" class="w-24 -ml-14  pb-2">
  @endif
</div>
