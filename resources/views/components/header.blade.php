<div class="flex justify-between items-center bg-gray-50 py-2 px-48 mb-4">
  <div>
    <a href="https://bicyclecards.com/how-to-play/blackjack/" target="_blank" rel="noopener noreferrer">
      How to Play
    </a>
  </div>
  <h1 class="font-gothic text-5xl tracking-wide uppercase text-center">
    <span class="text-black">Black</span>
    <span class="text-amber-800">jack</span>
  </h1>
  <div>{{ $cardsInShoe }} Cards in Shoe</div>
</div>
