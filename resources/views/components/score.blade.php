<div class="flex justify-between font-gothic text-4xl text-gray-700 tracking-wide uppercase border-b border-gray-400">
  <div class="text-gray-700">
    Score
  </div>
  <div class="text-amber-800">
    {{ $amount }}
  </div>
</div>
