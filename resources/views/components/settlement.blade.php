<div>
  @if ($messages)
    <div class="font-gothic text-4xl text-gray-700 tracking-wide uppercase border-b border-gray-400">
      Settlement
    </div>
    @foreach ($messages as $message)
      <div class="grid grid-cols-12 text-black font-nunito_regular border-b border-gray-400">
        <div class="col-span-8">
          {{ $message[0] }}
        </div>
        <div class="col-span-2">
          {{ $message[1] }}
        </div>
        <div class="col-span-2 text-right">
          {{ $message[2] }}
        </div>
      </div>
    @endforeach
  @endif
</div>
