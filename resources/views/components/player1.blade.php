<div class="border-b {{ $active ? 'border-amber-800' : 'border-gray-400' }}">
  <div class="flex justify-between font-gothic text-4xl text-gray-700 tracking-wide uppercase">
    <div class="{{ $active ? 'text-amber-800' : 'text-gray-700' }}">
      Player
    </div>
    <div>
      {{ $hand['status'] }}
    </div>
  </div>
  <div class="flex pl-14 {{ $inactive ? 'opacity-30' : '' }}">
    @if ($hand['cards'])
      @foreach ($hand['cards'] as $card)
        <img src="{{ 'https://images.bob-humphrey.com/cards/' . $card['name'] . '.png' }}" alt=""
          class="w-24 -ml-14  pb-2">
      @endforeach
    @else
      <img src="{{ 'https://images.bob-humphrey.com/cards/BLANK.png' }}" alt="" class="w-24 -ml-14  pb-2">
    @endif
  </div>
</div>
