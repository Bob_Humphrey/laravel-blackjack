<?php

use Illuminate\Support\Facades\Log;

define('DISABLED', 'DISABLED');
define('SHOE_MINIMUM_SIZE', 50);
define('TURN_NONE', 'NONE');
define('TURN_PLAYER', 'PLAYER');
define('TURN_DEALER', 'DEALER');
define('HAND_DEALER', 'DEALER');
define('HAND_PLAYER_FIRST', 'PLAYER-FIRST');
define('HAND_PLAYER_SECOND', 'PLAYER-SECOND');

function resetButtons()
{
  $buttons = [];
  $buttons['bet2Disabled'] = DISABLED;
  $buttons['bet10Disabled'] = DISABLED;
  $buttons['bet20Disabled'] = DISABLED;
  $buttons['bet50Disabled'] = DISABLED;
  $buttons['dealDisabled'] = DISABLED;
  $buttons['standDisabled'] = DISABLED;
  $buttons['hitDisabled'] = DISABLED;
  $buttons['splitDisabled'] = DISABLED;
  $buttons['doubleDisabled'] = DISABLED;
  $buttons['insureDisabled'] = DISABLED;
  return $buttons;
}

function setBetButtons($buttons, $state)
{
  $buttons['bet2Disabled'] = $state['score'] >= 2 &&
    $state['bet'] !== 2 ? '' : DISABLED;
  $buttons['bet10Disabled'] = $state['score'] >= 10 &&
    $state['bet'] !== 10 ? '' : DISABLED;
  $buttons['bet20Disabled'] = $state['score'] >= 20 &&
    $state['bet'] !== 20 ? '' : DISABLED;
  $buttons['bet50Disabled'] = $state['score'] >= 50 &&
    $state['bet'] !== 50 ? '' : DISABLED;
  return $buttons;
}

function adjustBet($state)
{
  if ($state['bet'] <= $state['score']) {
    return $state;
  }

  if ($state['score'] >= 20) {
    $state['bet'] = 20;
    $buttons = $state['buttons'];
    $state['buttons'] = setBetButtons($buttons, $state);
    return $state;
  }

  if ($state['score'] >= 10) {
    $state['bet'] = 10;
    $buttons = $state['buttons'];
    $state['buttons'] = setBetButtons($buttons, $state);
    return $state;
  }

  if ($state['score'] >= 2) {
    $state['bet'] = 2;
    $buttons = $state['buttons'];
    $state['buttons'] = setBetButtons($buttons, $state);
    return $state;
  }

  return $state;
}

function setPlayerActionButtons($state, $player)
{
  $buttons = resetButtons();
  $buttons['standDisabled'] = '';
  $buttons['hitDisabled'] = '';
  if ($state['turn'] === TURN_PLAYER) {
    $buttons['splitDisabled'] = $player['canSplit'] ? '' : DISABLED;
    $buttons['doubleDisabled'] = $player['canDouble'] ? '' : DISABLED;
    $buttons['insureDisabled'] = $player['canInsure'] ? '' : DISABLED;
  }
  return $buttons;
}

function initializeAllHands(bool $newHand, $state)
{
  // Initialize hands
  $dealerHand = $newHand ? NULL : $state['hands'][HAND_DEALER];
  $playerFirstHand = $newHand ? NULL : $state['hands'][HAND_PLAYER_FIRST];
  $playerSecondHand = $newHand ? NULL : $state['hands'][HAND_PLAYER_SECOND];
  $hands = [];
  $hand = getInitializedHand($dealerHand);
  $hands[HAND_DEALER] = $hand;
  $hand = getInitializedHand($playerFirstHand);
  $hands[HAND_PLAYER_FIRST] = $hand;
  $hand = getInitializedHand($playerSecondHand);
  $hands[HAND_PLAYER_SECOND] = $hand;
  $state['hands'] = $hands;
  return $hands;
}

function getInitializedHand($previousHand)
{
  $hand = [];
  $hand['cards'] = is_null($previousHand) ? [] : $previousHand['cards'];
  $hand['numberOfCards'] = 0;
  $hand['isNatural'] = FALSE;
  $hand['isBlackjack'] = FALSE;
  $hand['canSplit'] = FALSE;
  $hand['canDouble'] = FALSE;
  $hand['canInsure'] = FALSE;
  $hand['isBust'] = FALSE;
  $hand['aceCount'] = 0;
  $hand['lowTotal'] = 0;
  $hand['highTotal'] = 0;
  $hand['bestTotal'] = 0;
  $hand['status'] = '';
  return $hand;
}

function calculateBestTotal($aceCount, $highTotal)
{
  if ($highTotal <= 21) {
    return $highTotal;
  }

  if ($aceCount === 0) {
    return $highTotal;
  }

  while ($aceCount > 0) {
    $highTotal -= 10;
    if ($highTotal <= 21) {
      return $highTotal;
    }
    $aceCount--;
  }

  return $highTotal;
}
