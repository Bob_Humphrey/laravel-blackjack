<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Deal extends Component
{
  public $buttons;
  public $gameOver;

  // Listeners

  protected $listeners = ['refreshButtons'];

  public function refreshButtons($state)
  {
    $this->buttons = $state['buttons'];
    $this->gameOver = $state['gameOver'];
  }

  // Actions

  public function deal()
  {
    $this->emitUp('deal');
  }

  // Lifecycle Hooks

  public function mount($state)
  {
    $this->buttons = $state['buttons'];
    $this->gameOver = $state['gameOver'];
  }

  public function render()
  {
    return view('livewire.deal');
  }
}
