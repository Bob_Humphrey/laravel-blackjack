<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PlayHand extends Component
{
  public $buttons;
  public $gameOver;

  // Listeners

  protected $listeners = ['refreshButtons'];

  public function refreshButtons($state)
  {
    $this->buttons = $state['buttons'];
    $this->gameOver = $state['gameOver'];
  }

  // Actions

  public function stand()
  {
    $this->emitUp('stand');
  }

  public function hit()
  {
    $this->emitUp('hit');
  }

  public function split()
  {
    $this->emitUp('split');
  }

  public function double()
  {
    $this->emitUp('double');
  }

  public function insure()
  {
    $this->emitUp('insure');
  }

  // Lifecycle Hooks

  public function mount($state)
  {
    $this->buttons = $state['buttons'];
    $this->gameOver = $state['gameOver'];
  }
  public function render()
  {
    return view('livewire.play-hand');
  }
}
