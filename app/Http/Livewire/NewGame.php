<?php

namespace App\Http\Livewire;

use Livewire\Component;

class NewGame extends Component
{
  public $gameOver;

  // Listeners

  protected $listeners = ['refreshButtons'];

  public function refreshButtons($state)
  {
    $this->gameOver = $state['gameOver'];
  }

  // Actions

  public function newGame()
  {
    $this->emitUp('newGame');
  }

  // Lifecycle Hooks

  public function mount($state)
  {
    $this->gameOver = $state['gameOver'];
  }

  public function render()
  {
    return view('livewire.new-game');
  }
}
