<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Actions\BlackjackListeners\HitAction;
use App\Actions\BlackjackListeners\DealAction;
use App\Actions\Blackjack\InitializeGameAction;
use App\Actions\BlackjackListeners\SplitAction;
use App\Actions\BlackjackListeners\StandAction;
use App\Actions\BlackjackListeners\DoubleAction;
use App\Actions\BlackjackListeners\InsureAction;
use App\Actions\BlackjackListeners\SetBetAction;

class Blackjack extends Component
{
  public $state;

  // Listeners

  protected $listeners = ['setBet', 'deal', 'stand', 'hit', 'split', 'double', 'insure', 'newGame'];

  public function setBet($amount)
  {
    $state = $this->state;
    $state = SetBetAction::execute($amount, $state);
    $this->state = $state;
    $this->emit('refreshButtons', $state);
  }

  public function deal()
  {
    $state = $this->state;
    $state = DealAction::execute($state);
    $this->state = $state;
    $this->emit('refreshButtons', $state);
  }

  public function stand()
  {
    $state = $this->state;
    $state = StandAction::execute($state);
    $this->state = $state;
    $this->emit('refreshButtons', $state);
  }

  public function hit()
  {
    $state = $this->state;
    $state = HitAction::execute($state);
    $this->state = $state;
    $this->emit('refreshButtons', $state);
  }

  public function split()
  {
    $state = $this->state;
    $state = SplitAction::execute($state);
    $this->state = $state;
    $this->emit('refreshButtons', $state);
  }

  public function double()
  {
    $state = $this->state;
    $state = DoubleAction::execute($state);
    $this->state = $state;
    $this->emit('refreshButtons', $state);
  }

  public function insure()
  {
    $state = $this->state;
    $state = InsureAction::execute($state);
    $this->state = $state;
    $this->emit('refreshButtons', $state);
  }

  public function newGame()
  {
    $state = [];
    $state = InitializeGameAction::execute($state);
    $this->state = $state;
    $this->emit('refreshButtons', $state);
  }

  // Lifecycle Hooks

  public function mount()
  {
    $state = [];
    $state = InitializeGameAction::execute($state);
    $this->state = $state;
  }

  public function render()
  {
    return view('livewire.blackjack');
  }
}
