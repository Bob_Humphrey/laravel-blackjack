<?php

namespace App\Objects;

use Illuminate\Support\Arr;

class Deck
{
  public $names = [
    "AS",
    "2S",
    "3S",
    "4S",
    "5S",
    "6S",
    "7S",
    "8S",
    "9S",
    "0S",
    "JS",
    "QS",
    "KS",
    "AD",
    "2D",
    "3D",
    "4D",
    "5D",
    "6D",
    "7D",
    "8D",
    "9D",
    "0D",
    "JD",
    "QD",
    "KD",
    "AC",
    "2C",
    "3C",
    "4C",
    "5C",
    "6C",
    "7C",
    "8C",
    "9C",
    "0C",
    "JC",
    "QC",
    "KC",
    "AH",
    "2H",
    "3H",
    "4H",
    "5H",
    "6H",
    "7H",
    "8H",
    "9H",
    "0H",
    "JH",
    "QH",
    "KH",
  ];

  public $suits = [
    'S' => 'SPADES',
    'D' => 'DIAMONDS',
    'H' => 'HEARTS',
    'C' => 'CLUBS',
  ];

  public $values = [
    'A' => 'ACE',
    'K' => 'KING',
    'Q' => 'QUEEN',
    'J' => 'JACK',
    '0' => '10',
    '9' => '9',
    '8' => '8',
    '7' => '7',
    '6' => '6',
    '5' => '5',
    '4' => '4',
    '3' => '3',
    '2' => '2',
  ];

  public $points = [
    'A' => 11,
    'K' => 10,
    'Q' => 10,
    'J' => 10,
    '0' => 10,
    '9' => 9,
    '8' => 8,
    '7' => 7,
    '6' => 6,
    '5' => 5,
    '4' => 4,
    '3' => 3,
    '2' => 2,
  ];

  public $singleDeck;
  public $unshuffledShoe;

  public function __construct()
  {
    $this->singleDeck = [];
    $collection = collect($this->names);
    $collection->each(function ($item, $key) {
      $card = [];
      $card['name'] = $item;
      $card['suit'] = $this->suits[substr($item, 1)];
      $card['value'] = $this->values[substr($item, 0, 1)];
      $card['points'] = $this->points[substr($item, 0, 1)];
      $this->singleDeck[] = $card;
    });

    $this->unshuffledShoe = array_merge(
      $this->singleDeck,
      $this->singleDeck,
      $this->singleDeck,
      $this->singleDeck,
      $this->singleDeck,
      $this->singleDeck
    );
  }

  public function getShoe()
  {
    return Arr::shuffle($this->unshuffledShoe);
  }
}
