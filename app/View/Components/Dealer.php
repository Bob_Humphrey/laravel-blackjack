<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Dealer extends Component
{
  public $hand;
  public $showHoleCard;

  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($state)
  {
    $this->hand = $state['hands']['DEALER'];
    $this->showHoleCard = $state['showDealerHoleCard'];
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\Contracts\View\View|string
   */
  public function render()
  {
    return view('components.dealer');
  }
}
