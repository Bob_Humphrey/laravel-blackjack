<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Header extends Component
{
  public $cardsInShoe;

  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($state)
  {
    $cardsInShoe = count($state['shoe']);
    $cardsInShoe = $cardsInShoe === 0 ? 312 : $cardsInShoe;
    $this->cardsInShoe = $cardsInShoe;
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\Contracts\View\View|string
   */
  public function render()
  {
    return view('components.header');
  }
}
