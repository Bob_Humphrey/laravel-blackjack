<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Settlement extends Component
{
  public $messages;

  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($state)
  {
    $this->messages = $state['settlement'];
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\Contracts\View\View|string
   */
  public function render()
  {
    return view('components.settlement');
  }
}
