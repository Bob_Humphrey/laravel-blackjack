<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Player2 extends Component
{
  public $player;
  public $hand;
  public $active;
  public $inactive;

  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($state)
  {
    $this->player = $state['player'];
    $this->hand = $state['hands']['PLAYER-SECOND'];
    $this->active = FALSE;
    $this->inactive = FALSE;
    if ($state['turn'] !== 'PLAYER') {
      return;
    }
    if ($state['player']['currentHand'] === 2) {
      $this->active = TRUE;
    }
    if ($state['player']['currentHand'] === 1) {
      $this->inactive = TRUE;
    }
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\Contracts\View\View|string
   */
  public function render()
  {
    return view('components.player2');
  }
}
