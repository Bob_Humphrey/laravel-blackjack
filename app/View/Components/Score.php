<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Score extends Component
{
  public $amount;
  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($state)
  {
    $this->amount = $state['score'];
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\Contracts\View\View|string
   */
  public function render()
  {
    return view('components.score');
  }
}
