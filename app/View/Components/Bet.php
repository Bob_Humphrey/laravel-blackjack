<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Bet extends Component
{
  public $amount;
  public $player;

  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($state)
  {
    $this->amount = $state['bet'];
    $this->player = $state['player'];
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\Contracts\View\View|string
   */
  public function render()
  {
    return view('components.bet');
  }
}
