<?php

namespace App\Actions\BlackjackListeners;

use App\Actions\Blackjack\PlayerTurnAction;
use App\Actions\Blackjack\AnalyzeHandsAction;

class SplitAction
{
  public static function execute(array $state)
  {
    $hands = initializeAllHands(FALSE, $state);
    $state['hands'] = $hands;

    $card = array_pop($hands[HAND_PLAYER_FIRST]['cards']);
    array_push($hands[HAND_PLAYER_SECOND]['cards'], $card);
    $card = array_shift($state['shoe']);
    array_push($hands[HAND_PLAYER_FIRST]['cards'], $card);
    $card = array_shift($state['shoe']);
    array_push($hands[HAND_PLAYER_SECOND]['cards'], $card);
    $state['hands'] = $hands;

    $state['player']['numberOfHands'] = 2;

    $state = AnalyzeHandsAction::execute($state);
    $state = PlayerTurnAction::execute($state);
    return $state;
  }
}
