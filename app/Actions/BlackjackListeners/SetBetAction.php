<?php

namespace App\Actions\BlackjackListeners;

class SetBetAction
{
  public static function execute(int $amount, array $state)
  {
    $state['bet'] = $amount;
    $buttons = resetButtons();
    $buttons = setBetButtons($buttons, $state);
    $buttons['dealDisabled'] = '';
    $state['buttons'] = $buttons;
    return $state;
  }
}
