<?php

namespace App\Actions\BlackjackListeners;

use Illuminate\Support\Facades\Log;
use App\Actions\Blackjack\PlayerTurnAction;
use App\Actions\Blackjack\AnalyzeHandsAction;

class HitAction
{
  public static function execute(array $state)
  {
    $hands = initializeAllHands(FALSE, $state);
    $state['hands'] = $hands;

    $card = array_shift($state['shoe']);
    if ($state['player']['currentHand'] === 1) {
      array_push($state['hands'][HAND_PLAYER_FIRST]['cards'], $card);
    }
    if ($state['player']['currentHand'] === 2) {
      array_push($state['hands'][HAND_PLAYER_SECOND]['cards'], $card);
    }

    $state = AnalyzeHandsAction::execute($state);
    $state = PlayerTurnAction::execute($state);
    return $state;
  }
}
