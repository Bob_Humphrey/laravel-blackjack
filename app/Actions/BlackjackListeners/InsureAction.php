<?php

namespace App\Actions\BlackjackListeners;

class InsureAction
{
  public static function execute(array $state)
  {
    $state['player']['didInsure'] = TRUE;
    $state['player']['canInsure'] = FALSE;
    $state['buttons']['insureDisabled'] = DISABLED;
    return $state;
  }
}
