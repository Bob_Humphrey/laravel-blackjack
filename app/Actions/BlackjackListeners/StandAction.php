<?php

namespace App\Actions\BlackjackListeners;

use App\Actions\Blackjack\DealerTurnAction;
use App\Actions\Blackjack\PlayerTurnAction;
use App\Actions\Blackjack\AnalyzeHandsAction;

class StandAction
{
  public static function execute(array $state)
  {
    $hands = initializeAllHands(FALSE, $state);
    $state['hands'] = $hands;

    // Player has not split
    if ($state['player']['numberOfHands'] === 1) {
      $state = AnalyzeHandsAction::execute($state);
      $state = DealerTurnAction::execute($state);
      return $state;
    }

    // Player has split - first hand
    if ($state['player']['numberOfHands'] === 2 && $state['player']['currentHand'] === 1) {
      $state['player']['currentHand'] = 2;
      $state = AnalyzeHandsAction::execute($state);
      $state = PlayerTurnAction::execute($state);
      return $state;
    }

    // Player has split - second hand
    if ($state['player']['numberOfHands'] === 2 && $state['player']['currentHand'] === 2) {
      $state = AnalyzeHandsAction::execute($state);
      $state = DealerTurnAction::execute($state);
      return $state;
    }
  }
}
