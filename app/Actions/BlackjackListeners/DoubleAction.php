<?php

namespace App\Actions\BlackjackListeners;

use App\Actions\Blackjack\DealerTurnAction;
use App\Actions\Blackjack\AnalyzeHandsAction;

class DoubleAction
{
  public static function execute(array $state)
  {
    $state['player']['didDouble'] = TRUE;
    $state['player']['canDouble'] = FALSE;

    $hands = initializeAllHands(FALSE, $state);
    $state['hands'] = $hands;

    $card = array_shift($state['shoe']);
    array_push($state['hands'][HAND_PLAYER_FIRST]['cards'], $card);

    $state = AnalyzeHandsAction::execute($state);
    $state = DealerTurnAction::execute($state);
    return $state;
  }
}
