<?php

namespace App\Actions\BlackjackListeners;

use App\Actions\Blackjack\PlayerTurnAction;
use App\Actions\Blackjack\AnalyzeHandsAction;
use App\Actions\Blackjack\InitializeRoundAction;

class DealAction
{
  public static function execute(array $state)
  {
    $state = InitializeRoundAction::execute($state);
    $card = array_shift($state['shoe']);
    array_push($state['hands'][HAND_PLAYER_FIRST]['cards'], $card);
    $card = array_shift($state['shoe']);
    array_push($state['hands'][HAND_DEALER]['cards'], $card);
    $card = array_shift($state['shoe']);
    array_push($state['hands'][HAND_PLAYER_FIRST]['cards'], $card);
    $card = array_shift($state['shoe']);
    array_push($state['hands'][HAND_DEALER]['cards'], $card);
    $state = AnalyzeHandsAction::execute($state);
    $state = PlayerTurnAction::execute($state);
    return $state;
  }
}
