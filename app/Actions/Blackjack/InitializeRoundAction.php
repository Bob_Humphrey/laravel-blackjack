<?php

namespace App\Actions\Blackjack;

use App\Objects\Deck;

class InitializeRoundAction
{
  public static function execute(array $state)
  {
    $state['showDealerHoleCard'] = FALSE;

    $hands = initializeAllHands(TRUE, $state);
    $state['hands'] = $hands;

    if (count($state['shoe']) < SHOE_MINIMUM_SIZE) {
      $deck = new Deck();
      $state['shoe'] = $deck->getShoe();
    }

    $state['player']['numberOfHands'] = 1;
    $state['player']['currentHand'] = 1;
    $state['player']['hasNonBustedHand'] = TRUE;
    $state['player']['isNatural'] = FALSE;
    $state['player']['canSplit'] = FALSE;
    $state['player']['didSplit'] = FALSE;
    $state['player']['canDouble'] = FALSE;
    $state['player']['didDouble'] = FALSE;
    $state['player']['canInsure'] = FALSE;
    $state['player']['didInsure'] = FALSE;

    $state['turn'] = TURN_PLAYER;
    $state['settlement'] = [];
    return $state;
  }
}
