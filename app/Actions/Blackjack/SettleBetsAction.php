<?php

namespace App\Actions\Blackjack;

use Illuminate\Support\Facades\Log;

class SettleBetsAction
{
  public static function execute(array $state)
  {
    $settlement = [];
    $state['showDealerHoleCard'] = TRUE;
    $doubleValue = $state['player']['didDouble'] ? 2 : 1;
    $playerFirstHand = $state['player']['numberOfHands'] === 1 ? 'Player' : 'Player first hand';
    $score = $state['score'];
    $bet = $state['bet'];
    $settlementItem = ['Old score', '', $state['score']];
    $settlement[] = $settlementItem;

    // Insurance
    if ($state['player']['didInsure']) {
      if ($state['hands'][HAND_DEALER]['isBlackjack']) {
        $score += $bet / 2;
        $settlementItem = ['Insurance', 'Win', $bet / 2];
        $settlement[] = $settlementItem;
      } else {
        $score -= $bet / 2;
        $settlementItem = ['Insurance', 'Lose', $bet / 2];
        $settlement[] = $settlementItem;
      }
    }

    // Natural
    if ($state['player']['isNatural']) {
      if ($state['hands'][HAND_DEALER]['isBlackjack']) {
        $state['hands'][HAND_DEALER]['status'] = 'Blackjack';
        $state['hands'][HAND_PLAYER_FIRST]['status'] = 'Blackjack';
        $settlementItem = ['Natural', 'Push', 0];
        $settlement[] = $settlementItem;
      } else {
        $state['hands'][HAND_DEALER]['status'] =
          $state['hands'][HAND_DEALER]['isBust'] ?
          'Bust' : $state['hands'][HAND_DEALER]['bestTotal'];
        $state['hands'][HAND_PLAYER_FIRST]['status'] = 'Blackjack';
        $score += $bet * 1.5;
        $settlementItem = ['Natural', 'Win', $bet * 1.5];
        $settlement[] = $settlementItem;
      }

      $settlement[] = ['New score', '', $score];
      $state['score'] = $score;
      $state['settlement'] = $settlement;
      $state['turn'] = TURN_NONE;

      $buttons = resetButtons();
      $buttons = setBetButtons($buttons, $state);
      $buttons['dealDisabled'] = '';
      $state['buttons'] = $buttons;
      $state = adjustBet($state);

      return $state;
    }

    // Player first hand bust
    if ($state['hands'][HAND_PLAYER_FIRST]['isBust']) {
      $state['hands'][HAND_DEALER]['status'] =
        $state['hands'][HAND_DEALER]['bestTotal'];
      $state['hands'][HAND_PLAYER_FIRST]['status'] = 'Bust';
      $score -= $bet * $doubleValue;
      $settlementItem = [$playerFirstHand . ' bust', 'Lose', $bet * $doubleValue];
      $settlement[] = $settlementItem;
    }

    // Player second hand bust
    if ($state['hands'][HAND_PLAYER_SECOND]['isBust']) {
      $state['hands'][HAND_PLAYER_SECOND]['status'] = 'Bust';
      $score -= $bet * $doubleValue;
      $settlementItem = ['Player second hand bust', 'Lose', $bet * $doubleValue];
      $settlement[] = $settlementItem;
    }

    // Dealer bust - player first hand
    if (($state['hands'][HAND_DEALER]['isBust']) &&
      (!$state['hands'][HAND_PLAYER_FIRST]['isBust'])
    ) {
      $state['hands'][HAND_DEALER]['status'] = 'Bust';
      $state['hands'][HAND_PLAYER_FIRST]['status'] =
        $state['hands'][HAND_PLAYER_FIRST]['bestTotal'];
      $score += $bet * $doubleValue;
      $settlementItem = [$playerFirstHand . ' wins - dealer bust', 'Win', $bet * $doubleValue];
      $settlement[] = $settlementItem;
    }

    // Dealer bust - player second hand
    if (($state['player']['numberOfHands'] === 2) &&
      ($state['hands'][HAND_DEALER]['isBust']) &&
      (!$state['hands'][HAND_PLAYER_SECOND]['isBust'])
    ) {
      $state['hands'][HAND_PLAYER_SECOND]['status'] =
        $state['hands'][HAND_PLAYER_SECOND]['bestTotal'];
      $score += $bet * $doubleValue;
      $settlementItem = ['Player second hand winds - dealer bust', 'Win', $bet * $doubleValue];
      $settlement[] = $settlementItem;
    }

    // No bust - dealer hand > player first hand
    if ((!$state['hands'][HAND_DEALER]['isBust']) &&
      (!$state['hands'][HAND_PLAYER_FIRST]['isBust']) &&
      ($state['hands'][HAND_DEALER]['bestTotal'] >
        $state['hands'][HAND_PLAYER_FIRST]['bestTotal'])
    ) {
      $state['hands'][HAND_DEALER]['status'] =
        $state['hands'][HAND_DEALER]['isBlackjack'] ?
        'Blackjack' : $state['hands'][HAND_DEALER]['bestTotal'];
      $state['hands'][HAND_PLAYER_FIRST]['status'] =
        $state['hands'][HAND_PLAYER_FIRST]['bestTotal'];
      $score -= $bet * $doubleValue;
      $settlementItem = [$playerFirstHand . ' loses', 'Lose', $bet * $doubleValue];
      $settlement[] = $settlementItem;
    }

    // No bust - dealer hand < player first hand
    if ((!$state['hands'][HAND_DEALER]['isBust']) &&
      (!$state['hands'][HAND_PLAYER_FIRST]['isBust']) &&
      ($state['hands'][HAND_DEALER]['bestTotal'] <
        $state['hands'][HAND_PLAYER_FIRST]['bestTotal'])
    ) {
      $state['hands'][HAND_DEALER]['status'] =
        $state['hands'][HAND_DEALER]['bestTotal'];
      $state['hands'][HAND_PLAYER_FIRST]['status'] =
        $state['hands'][HAND_PLAYER_FIRST]['isBlackjack'] ?
        'Blackjack' : $state['hands'][HAND_PLAYER_FIRST]['bestTotal'];
      $score += $bet * $doubleValue;
      $settlementItem = [$playerFirstHand . ' wins', 'Win', $bet * $doubleValue];
      $settlement[] = $settlementItem;
    }

    // No bust - dealer hand === player first hand
    if ((!$state['hands'][HAND_DEALER]['isBust']) &&
      (!$state['hands'][HAND_PLAYER_FIRST]['isBust']) &&
      ($state['hands'][HAND_DEALER]['bestTotal'] ===
        $state['hands'][HAND_PLAYER_FIRST]['bestTotal'])
    ) {
      $state['hands'][HAND_DEALER]['status'] =
        $state['hands'][HAND_DEALER]['isBlackjack'] ?
        'Blackjack' : $state['hands'][HAND_DEALER]['bestTotal'];
      $state['hands'][HAND_PLAYER_FIRST]['status'] =
        $state['hands'][HAND_PLAYER_FIRST]['isBlackjack'] ?
        'Blackjack' : $state['hands'][HAND_PLAYER_FIRST]['bestTotal'];
      $settlementItem = [$playerFirstHand, 'Push', 0];
      $settlement[] = $settlementItem;
    }

    // No bust - dealer hand > player second hand
    if (($state['player']['numberOfHands'] === 2) &&
      (!$state['hands'][HAND_DEALER]['isBust']) &&
      (!$state['hands'][HAND_PLAYER_SECOND]['isBust']) &&
      ($state['hands'][HAND_DEALER]['bestTotal'] >
        $state['hands'][HAND_PLAYER_SECOND]['bestTotal'])
    ) {
      $state['hands'][HAND_DEALER]['status'] =
        $state['hands'][HAND_DEALER]['bestTotal'];
      $state['hands'][HAND_PLAYER_SECOND]['status'] =
        $state['hands'][HAND_DEALER]['isBlackjack'] ?
        'Blackjack' : $state['hands'][HAND_PLAYER_SECOND]['bestTotal'];
      $score -= $bet * $doubleValue;
      $settlementItem = ['Player second hand loses', 'Lose', $bet * $doubleValue];
      $settlement[] = $settlementItem;
    }

    // No bust - dealer hand < player second hand
    if (($state['player']['numberOfHands'] === 2) &&
      (!$state['hands'][HAND_DEALER]['isBust']) &&
      (!$state['hands'][HAND_PLAYER_SECOND]['isBust']) &&
      ($state['hands'][HAND_DEALER]['bestTotal'] <
        $state['hands'][HAND_PLAYER_SECOND]['bestTotal'])
    ) {
      $state['hands'][HAND_DEALER]['status'] =
        $state['hands'][HAND_DEALER]['bestTotal'];
      $state['hands'][HAND_PLAYER_SECOND]['status'] =
        $state['hands'][HAND_PLAYER_SECOND]['isBlackjack'] ?
        'Blackjack' : $state['hands'][HAND_PLAYER_SECOND]['bestTotal'];
      $score += $bet * $doubleValue;
      $settlementItem = ['Player second hand wins', 'Win', $bet * $doubleValue];
      $settlement[] = $settlementItem;
    }

    // No bust - dealer hand === player second hand
    if (($state['player']['numberOfHands'] === 2) &&
      (!$state['hands'][HAND_DEALER]['isBust']) &&
      (!$state['hands'][HAND_PLAYER_SECOND]['isBust']) &&
      ($state['hands'][HAND_DEALER]['bestTotal'] ===
        $state['hands'][HAND_PLAYER_SECOND]['bestTotal'])
    ) {
      $state['hands'][HAND_DEALER]['status'] =
        $state['hands'][HAND_DEALER]['isBlackjack'] ?
        'Blackjack' : $state['hands'][HAND_DEALER]['bestTotal'];
      $state['hands'][HAND_PLAYER_SECOND]['status'] =
        $state['hands'][HAND_PLAYER_SECOND]['isBlackjack'] ?
        'Blackjack' : $state['hands'][HAND_PLAYER_SECOND]['bestTotal'];
      $settlementItem = ['Player second hand', 'Push', 0];
      $settlement[] = $settlementItem;
    }

    $settlement[] = ['New score', '', $score];
    $state['score'] = $score;
    $state['settlement'] = $settlement;
    $state['turn'] = TURN_NONE;

    $buttons = resetButtons();
    $buttons = setBetButtons($buttons, $state);
    $buttons['dealDisabled'] = '';
    $state['buttons'] = $buttons;

    $state = adjustBet($state);
    $state['gameOver'] = $score <= 0 ? TRUE : FALSE;
    return $state;
  }
}
