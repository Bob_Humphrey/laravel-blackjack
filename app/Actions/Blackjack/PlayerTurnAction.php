<?php

namespace App\Actions\Blackjack;

class PlayerTurnAction
{
  public static function execute(array $state)
  {
    $player = $state['player'];
    $fundsAvailable = $state['score'] - $state['bet'];

    if ($player['currentHand'] === 1) {
      $player['isNatural'] = $state['hands'][HAND_PLAYER_FIRST]['isNatural'];
      $player['canSplit'] = $state['hands'][HAND_PLAYER_FIRST]['canSplit'];
      $player['canDouble'] = $state['hands'][HAND_PLAYER_FIRST]['canDouble'];
      $player['canInsure'] =
        ($state['hands'][HAND_DEALER]['cards'][0]['value'] === 'ACE' && $state['hands'][HAND_PLAYER_FIRST]['canInsure']) ?
        TRUE : FALSE;
      $player['hasNonBustedHand'] = $state['hands'][HAND_PLAYER_FIRST]['isBust'] ? FALSE : TRUE;
      // Funds must be available to split, double, or insure. 
      if ($fundsAvailable < $state['bet']) {
        $player['canSplit'] = FALSE;
        $player['canDouble'] = FALSE;
      }
      if ($fundsAvailable < ($state['bet'] / 2)) {
        $player['canInsure'] = FALSE;
      }
    }

    $state['player'] = $player;

    // Player first hand

    if ($player['currentHand'] === 1) {

      // Natural
      if ($player['isNatural']) {
        $state = SettleBetsAction::execute($state);
        return $state;
      }

      // Blackjack 
      elseif ($state['hands'][HAND_PLAYER_FIRST]['isBlackjack']) {
        $state['player']['currentHand'] = 2;
      }

      // Bust
      elseif ($state['hands'][HAND_PLAYER_FIRST]['isBust']) {
        if ($state['player']['numberOfHands'] === 1) {
          $state['player']['hasNonBustedHand'] = FALSE;
          $state = SettleBetsAction::execute($state);
          return $state;
        } else {
          $state['player']['currentHand'] = 2;
        }
      }

      // Set up next player decision.
      $state['buttons'] = setPlayerActionButtons($state, $player);
      return $state;
    }

    // Player second hand

    if ($player['currentHand'] === 2) {

      // Blackjack 
      if ($state['hands'][HAND_PLAYER_SECOND]['isBlackjack']) {
        if ($state['hands'][HAND_PLAYER_FIRST]['isBlackjack']) {
          $state = SettleBetsAction::execute($state);
          return $state;
        } elseif ($state['hands'][HAND_PLAYER_FIRST]['isBust']) {
          $state = SettleBetsAction::execute($state);
          return $state;
        } else {
          $state = DealerTurnAction::execute($state);
          return $state;
        }
      }

      // Bust 
      if ($state['hands'][HAND_PLAYER_SECOND]['isBust']) {
        if ($state['hands'][HAND_PLAYER_FIRST]['isBlackjack']) {
          $state['player']['hasNonBustedHand'] = TRUE;
          $state = DealerTurnAction::execute($state);
          return $state;
        } elseif ($state['hands'][HAND_PLAYER_FIRST]['isBust']) {
          $state['player']['hasNonBustedHand'] = FALSE;
          $state = SettleBetsAction::execute($state);
          return $state;
        } else {
          $state = DealerTurnAction::execute($state);
          return $state;
        }
      }

      // Set up next player decision.
      $state['buttons'] = setPlayerActionButtons($state, $player);
      return $state;
    }
  }
}
