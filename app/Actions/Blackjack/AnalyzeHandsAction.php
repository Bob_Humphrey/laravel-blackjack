<?php

namespace App\Actions\Blackjack;

class AnalyzeHandsAction
{
  public static function execute(array $state)
  {
    $hands = [];
    foreach ($state['hands'] as $key => $hand) {
      foreach ($hand['cards'] as $card) {
        $hand['numberOfCards']++;
        $hand['aceCount'] = $card['value'] === 'ACE' ? $hand['aceCount'] + 1 : $hand['aceCount'];
        $hand['highTotal'] += $card['points'];
        $hand['lowTotal'] = $hand['highTotal'] - ($hand['aceCount'] * 10);
        $hand['bestTotal'] = calculateBestTotal($hand['aceCount'], $hand['highTotal']);
      }

      $hand['isBust'] = $hand['bestTotal'] > 21 ? TRUE : FALSE;
      $hand['isBlackjack'] =
        ($hand['highTotal'] === 21 && $hand['numberOfCards']  === 2) ?
        TRUE : FALSE;

      if ($key === HAND_PLAYER_FIRST) {
        $hand['isNatural'] = $hand['isBlackjack'] ? TRUE : FALSE;
        if ($hand['numberOfCards'] === 2 && $state['player']['numberOfHands'] === 1) {
          $hand['canSplit'] =
            ($hand['cards'][0]['value'] === $hand['cards'][1]['value']) ?
            TRUE : FALSE;
          $hand['canDouble'] =
            ($hand['highTotal'] === 9  || $hand['highTotal'] === 10 || $hand['highTotal'] === 11 || $hand['lowTotal'] === 10) ?
            TRUE : FALSE;
          $hand['canInsure'] = TRUE;
        }
      }

      $hands[$key] = $hand;
    }
    $state['hands'] = $hands;
    return $state;
  }
}
