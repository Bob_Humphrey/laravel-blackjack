<?php

namespace App\Actions\Blackjack;

class DealerTurnAction
{
  public static function execute(array $state)
  {
    $state['showDealerHoleCard'] = TRUE;
    $dealerTurn = TRUE;
    while ($dealerTurn === TRUE) {
      if ($state['hands'][TURN_DEALER]['bestTotal'] < 17) {
        // Dealer draws if hand is less than 17. 

        // Initialize hands
        $hands = initializeAllHands(FALSE, $state);
        $state['hands'] = $hands;

        // Draw a card
        $card = array_shift($state['shoe']);
        array_push($state['hands'][HAND_DEALER]['cards'], $card);

        $state = AnalyzeHandsAction::execute($state);
      } else {
        // Dealer stands if hand is >= 17. 
        $dealerTurn = FALSE;
      }
    }

    $state = SettleBetsAction::execute($state);
    return $state;
  }
}
