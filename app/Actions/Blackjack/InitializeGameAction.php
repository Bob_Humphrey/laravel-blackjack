<?php

namespace App\Actions\Blackjack;

class InitializeGameAction
{
  public static function execute(array $state)
  {
    $state['shoe'] = [];
    $state['score'] = 1000;
    $state['bet'] = 2;
    $state['showDealerHoleCard'] = FALSE;
    $state = InitializeRoundAction::execute($state);
    $buttons = resetButtons();
    $buttons = setBetButtons($buttons, $state);
    $buttons['dealDisabled'] = '';
    $state['buttons'] = $buttons;
    $state['turn'] = TURN_NONE;
    $state['gameOver'] = FALSE;
    return $state;
  }
}
